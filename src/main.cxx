#include <string>
#include <utility>
#include <variant>
#include <iostream>
#include <vector>

// File parsing
std::string load(const std::string &);

// CSV operations
namespace csv {
std::vector<std::vector<std::variant<double, std::string>>>
read(const std::string &);
std::vector<std::vector<double>>
columns(const std::vector<std::vector<std::variant<double, std::string>>> &);
} // namespace csv

std::string summary(const std::string &, const std::vector<double> &);
std::vector<std::pair<double, double>> zip(const std::vector<double> &);
std::vector<std::pair<double, double>>
normalise(const std::vector<std::pair<double, double>> &);
std::string xy(const std::vector<std::pair<uint32_t, uint32_t>> &);

std::vector<std::pair<uint32_t, uint32_t>>
interpolate(const std::vector<std::pair<double, double>> &);

int main(int argc, char **argv) {

  // Get file name from command line and store the contents
  const auto name = std::string{argc == 2 ? argv[1] : ""};
  const auto file = load(name);

  // Parse as a CSV
  const auto csv = csv::read(file);
  const auto columns = csv::columns(csv);

  // Plot each series
  for (const auto &series : columns) {
    const auto info = summary(name, series);
    const auto zipped = zip(series);
    const auto normalised = normalise(zipped);
    const auto interpolated = interpolate(normalised);
    const auto plot = xy(interpolated);
    std::cout << info << plot;
  }
}
