#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

// CSV processing

namespace csv {

// Extract a row of hererogenous types from a single line of text
auto read_row(const std::string &in) {
  std::vector<std::variant<double, std::string>> out;

  std::stringstream ss(in);
  std::string cell;
  while (std::getline(ss, cell, ',')) {
    // Store recognised types
    std::stringstream ss_cell(cell);
    if (double double_value; ss_cell >> double_value)
      out.emplace_back(double_value);
    else
      out.emplace_back(ss_cell.str());
  }

  return out;
}

// Take a CSV file name as a string and return 2-D container of cells
std::vector<std::vector<std::variant<double, std::string>>>
read(const std::string &in) {

  std::vector<std::vector<std::variant<double, std::string>>> out;

  // Parse input data line by line, empty rows are skipped
  std::stringstream ss(in);
  std::string line;
  while (std::getline(ss, line))
    if (const auto row = read_row(line); not row.empty())
      out.push_back(row);

  return out;
}

// Extract all numeric cells from a row
auto get_numerics(const std::vector<std::variant<double, std::string>> &in) {

  std::vector<double> out;

  // Copy only numeric cells into output container
  std::ranges::for_each(in, [&](const auto &cell) {
    if (std::holds_alternative<double>(cell))
      out.push_back(std::get<double>(cell));
  });

  return out;
}

// Return numeric columns from a table
std::vector<std::vector<double>>
columns(const std::vector<std::vector<std::variant<double, std::string>>> &in) {

  std::vector<std::vector<double>> out{{}};

  for (const auto &row : in) {

    const auto numerics = get_numerics(row);

    // Create a new row or add to an existing one
    for (size_t i{0}; const auto &cell : numerics) {

      if (out.size() < (i + 1))
        out.push_back({});

      // Push value onto existing or newly created column
      out.at(i).push_back(cell);
      ++i;
    }
  }

  return out;
}
} // namespace csv

#ifndef NDEBUG
#include <cassert>
#include <fmt/core.h>
#include <fmt/ranges.h>
int main() {
  const std::string in{
      R"(
Thu 18 Nov 20:35:02 UTC 2021, 43.3, 0.9849209436560172
Thu 18 Nov 20:36:03 UTC 2021, 43.3, 0.9929276460418445
Thu 18 Nov 20:37:02 UTC 2021, 43.3, 0.9672497562148542
Thu 18 Nov 20:38:02 UTC 2021, 43.3, 0.9679888886727708
Thu 18 Nov 20:39:02 UTC 2021, 42.8, 0.9471980746551188
Thu 18 Nov 20:40:02 UTC 2021, 42.2, 0.9663530330865822
Thu 18 Nov 20:41:02 UTC 2021, 42.8, 0.9595102333253747

Thu 18 Nov 20:42:02 UTC 2021, 42.2, 0.9447648793778042
Thu 18 Nov 20:43:03 UTC 2021, 42.8, 0.9630523718690964
Thu 18 Nov 20:44:03 UTC 2021, 42.8, 0.9713585347224767
Thu 18 Nov 20:45:03 UTC 2021, 42.8, 0.965317590352964
Thu 18 Nov 20:46:02 UTC 2021, 42.8, 0.9721583693284604
Thu 18 Nov 20:47:02 UTC 2021, 42.8, 0.9423332346555858
Thu 18 Nov 20:48:02 UTC 2021, 42.8, 0.9611319859583978  
)"};

  // Test CSV parser
  const auto out = csv::read(in);
  assert(out.size() == 14);
  assert(out.front().size() == 3);

  // Test row read
  const auto numeric_row = csv::get_numerics({{"hi"}, {0.0}, {0.1}});
  fmt::print("Numeric row: {}\n", numeric_row);
  assert(csv::get_numerics({{"hi"}, {0.0}, {0.1}}).size() == 2);
  assert(csv::get_numerics({{"hi"}, {"holla"}}).empty());

  // Test column read
  const auto cols = csv::columns(out);
  assert(not cols.empty());
  assert(cols.size() == 2);
  assert(cols.at(0).size() == 14);

  for (size_t i{0}; const auto &col : cols)
    fmt::print("Column {}: {}\n", i++, col);
}
#endif
