#include <algorithm>
#include <ranges>
#include <utility>
#include <vector>

// Generate X-axis values for a single series
std::vector<std::pair<double, double>> zip(const std::vector<double> &in) {

  const auto z = [x = 0.0](const auto &y) mutable {
    return std::make_pair(x++, y);
  };

  std::vector<std::pair<double, double>> out;
  for (const auto &p : in | std::views::transform(z))
    out.push_back(p);

  return out;
}

#ifndef NDEBUG
#include <cassert>
#include <fmt/core.h>
int main() {

  // Check empty input doesn't blow up
  assert(zip({}).empty());

  const auto in = std::vector<double>{12536.1, 17562.2, 32448.3, 15937.4,
                                      25990.5, 14567.7, 15864.8, 23788.9};

  const auto out = zip(in);
  assert(in.size() == out.size());
  assert(out.front().first == 0);
  assert(out.back().first == out.size() - 1ul);

  for (const auto &[x, y] : out)
    fmt::print("[{}, {}]\n", x, y);
}
#endif
