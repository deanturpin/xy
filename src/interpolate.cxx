#include <algorithm>
#include <cmath>
#include <cstdint>
#include <ranges>
#include <string>
#include <utility>
#include <vector>

// Debug routine to check it's all good before passing to the full plotter
std::string preview_plot(const std::vector<std::pair<uint32_t, uint32_t>> &in) {

  auto out = std::string{};

  // Construct a rotated plot (because it's easier)
  std::ranges::for_each(in, [&](const auto &p) {
    const auto &[x, y] = p;
    const auto line = std::string(1 + y, '-');
    out += std::to_string(x) + "\t" + line + "|" + std::to_string(y) + "\n";
  });

  return out;
}

// Take a container of normalised cooridinates and rescale to output resolution
std::vector<std::pair<uint32_t, uint32_t>>
interpolate(const std::vector<std::pair<double, double>> &in) {

  if (in.empty())
    return {};

  const auto W = double{190};
  const auto H = double{40};

  std::vector<std::pair<uint32_t, uint32_t>> out;

  auto lower = std::begin(in);
  for (const auto x : std::views::iota(0) | std::views::take(W)) {

    auto upper = std::begin(in);
    auto it = lower;

    // Find bounds
    while (it != std::end(in)) {
      if (it->first <= x / W) {
        lower = it;
        upper = std::next(it);
      }

      ++it;
    }

    const auto distance = upper->first - lower->first;
    const auto gap = (x - (W * lower->first)) / (W * distance);
    const auto interpolated = std::clamp(
        std::lerp(lower->second, upper->second, gap) * H, 0.0, H - 1.0);
    out.emplace_back(x, static_cast<uint32_t>(std::trunc(interpolated)));
  }

  return out;
}

#ifndef NDEBUG
#include <fmt/core.h>
#include <cassert>
int main() {

  // Check empty input doesn't blow up
  const auto empty = interpolate({});

  const auto in1 =
      std::vector<std::pair<double, double>>{{0, 1}, {0.01, 0}, {1, 1}};

  const auto in2 = std::vector<std::pair<double, double>>{
      {0, 0}, {0.3333333333333333, 0.5}, {0.6666666666666666, 1}, {1, 0}};

  const auto out1 = interpolate(in1);
  const auto out2 = interpolate(in2);

  fmt::print("{}\n", preview_plot(out1));
  fmt::print("{}\n", preview_plot(out2));
}
#endif
