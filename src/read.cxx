#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// Read data from either a file or stdin and return the contents as a string
std::string load(const std::string &in) {

  std::stringstream ss;

  if (in.empty())
    ss << std::cin.rdbuf();
  else {
    std::ifstream file{in};
    ss << file.rdbuf();
  }

  return ss.str();
}

#ifndef NDEBUG
#include <cassert>
int main() { assert(load("").empty()); }
#endif
