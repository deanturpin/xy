Example usage: https://covid.germs.dev

Exploring new C++ features and Linux packaging whilst developing a command line ASCII plotter. Intended for quick inspection of CSV files and as a confidence tool in CI jobs. Emphasis is on C++ features and describing intention through use of the language rather than particularly performant code (at the end of the day this is just dumping strings to the terminal). 

# Objectives
- Simple API: can take files or stdin
- Robust: attempt to do _something_ with anything you pass to it 
- Develop using online tools: GitLab and Godbolt
- Pure C++: no dependency on third party libraries (unless they are due to be included in the Standard Library: e.g., `<format>`)
- Track the latest Ubuntu (`ubuntu:devel`)
- Understand the difficulties of making _everything_ unit testable

## Usage
Take a filename:
```bash
$ xy live.csv
```

Take stdin:
```bash
$ cat live.csv | xy
```

# Architecture
```mermaid
graph TD
    main -->|arg present| read --> get_columns
    main -->|arg NOT present| stream --> get_columns
    get_columns --> zip --> normalise --> interpol --> plot
    get_columns --> summary --> OUTPUT
    plot --> OUTPUT
```

# Godbolt
- All files compile and run standalone in Godbolt
- `clang-format` applied in the editor

## Tricky bugs
Whilst I've aspired to keep every file modular enough to run standalone in Godbolt, for some tricky bugs I had to run the full application in `gdb`. But perhaps if the test cases were more comprehensive this would be less likely?

# C++ features
## C++23
- `std::string::contains()`

## C++20
- `std::ranges`
- `std::views::iota`
- `std::views::transform`
