#include <algorithm>
#include <utility>
#include <vector>

// Take a container of coordinates and normalise to fall between 0.0 and 1.0
std::vector<std::pair<double, double>>
normalise(const std::vector<std::pair<double, double>> &in) {

  // Get limits of these data
  const auto &[min_x, max_x] = std::ranges::minmax_element(
      in, [](const auto &a, const auto &b) { return a.first < b.first; });

  const auto &[min_y, max_y] = std::ranges::minmax_element(
      in, [](const auto &a, const auto &b) { return a.second < b.second; });

  std::vector<std::pair<double, double>> out;

  // Normalise each coordinate
  std::ranges::for_each(in, [&](const auto &p) {
    const auto &[x, y] = p;

    // Don't spread very small ranges over the whole Y range
    const auto range_y = std::max(0.2, max_y->second - min_y->second);

    // Normalise coordinate between 0 and 1
    const auto norm_x = (x - min_x->first) / (max_x->first - min_x->first);
    const auto norm_y = (y - min_y->second) / range_y;

    out.emplace_back(norm_x, norm_y);
  });

  return out;
}

#ifndef NDEBUG
#include <cassert>
#include <fmt/core.h>
#include <fmt/ranges.h>
int main() {

  fmt::print("Two coords\n");
  const auto in = std::vector<std::pair<double, double>>{{0, 0}, {10, 0.2}};
  const auto expected = std::vector<std::pair<double, double>>{{0, 0}, {1, 1}};

  const auto out = normalise(in);
  for (const auto &[x, y] : out)
    fmt::print("{{{}, {}}},\n", x, y);

  assert(out == expected);
  assert(out.back().first == 1.0);

  fmt::print("Six coords\n");
  for (const auto &[x, y] :
       normalise({{0, 10}, {1, 20}, {2, 50}, {3, -10}, {4, 1}, {5, 1}, {6, 1}}))
    fmt::print("{{{}, {}}},\n", x, y);
}
#endif
