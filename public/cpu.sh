# Log files where we keep the CPU stats
readonly log1=/tmp/xy-cpu-log1.csv
readonly log2=/tmp/xy-cpu-log2.csv

touch $log1 $log2

# Get the CPU usage periodically
if [[ $(uptime) =~ load\ average:\ ([^,]+) ]]; then
	cpu=${BASH_REMATCH[1]}
	cpu2=$(bc <<< "$cpu * 100 / $(nproc)")
	cat $log1 <(echo $cpu2) | tail -80 | tee $log2
	mv $log2 $log1
fi

